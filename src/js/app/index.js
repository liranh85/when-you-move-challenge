import React, { Component } from 'react';
import IconImage from 'images/icon.png';
import FruitMachine from '../components/FruitMachine';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='hero'>
        <h1>FRUIT MACHINE</h1>
        <FruitMachine numColors={3} />
      </div>
    )
  }
}

export default App