import React, { Component } from 'react';
import PropTypes from 'prop-types';

class FruitMachine extends Component {
    constructor(props) {
        super(props);

        this.data = {
            colors: ['red', 'blue', 'green', 'yellow']
        }

        this.state = {
            resultingColors: []
        }

        this.clickPlayHandler = this.clickPlayHandler.bind(this);
        this.generateRandomNumber = this.generateRandomNumber.bind(this);
        this.isAllValuesEqual = this.isAllValuesEqual.bind(this);
    }

    isAllValuesEqual(arr) {
        return arr.every(value => value === arr[0]);
    }

    generateRandomNumber(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    clickPlayHandler() {
        const results = [];
        let resultingColors;
        for(let i = 0; i < this.props.numColors; i++) {
            results.push(this.generateRandomNumber(0, this.data.colors.length));
        }
        resultingColors = results.map(value => this.data.colors[value]);
        this.setState({resultingColors});
        if(this.isAllValuesEqual(resultingColors)) {
            alert("Ding ding ding! You win!");
        }
    }

    render() {
        let resultingColors = '';
        if(this.state.resultingColors.length > 0) {
            resultingColors = this.state.resultingColors.map((value, index) => {
                return <div className="color-result" style={{backgroundColor: value}} key={index} />
            });
        }
        return (
            <div className="fruit-machine">
                <button onClick={this.clickPlayHandler}>Play</button>
                {resultingColors}
            </div>
        );
    }
}

FruitMachine.propTypes = {
    numColors: PropTypes.number.isRequired
}

export default FruitMachine;